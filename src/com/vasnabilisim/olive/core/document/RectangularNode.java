package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.Insets;
import com.vasnabilisim.olive.core.paint.Border;
import com.vasnabilisim.olive.core.property.BorderProperty;
import com.vasnabilisim.olive.core.property.InsetsProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;

/**
 * All rectangular item are of this type.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class RectangularNode extends AreaNode {
	private static final long serialVersionUID = -1789634950873687040L;

	protected BorderProperty borderProperty;
	protected InsetsProperty paddingProperty;
	
	public RectangularNode() {
		super();
		this.borderProperty = PropertyFactoryX.createBorderProperty("Content", "Border", "Border", "Border to draw aound element", Border.Empty.clone());
		this.paddingProperty = PropertyFactoryX.createInsetsProperty("Content", "Padding", "Padding", "Padding left, top, right, bottom", Insets.Zero.clone());
		this.properties.add(this.borderProperty);
		this.properties.add(this.paddingProperty);
	}

	public RectangularNode(RectangularNode source) {
		super(source);
		this.borderProperty = source.borderProperty.clone();
		this.paddingProperty = source.paddingProperty.clone();
		this.properties.add(this.borderProperty);
		this.properties.add(this.paddingProperty);
	}
	
	@Override
	public abstract RectangularNode clone();
	
	public Border getBorder() {
		return borderProperty.getValue();
	}
	
	public void setBorder(Border value) {
		borderProperty.setValue(value);
	}
	
	public Insets getPadding() {
		return paddingProperty.getValue();
	}
	
	public void setPadding(Insets value) {
		paddingProperty.setValue(value);
	}
	
	@Override
	public Dimension calcMinimumSize(DocumentContext context) {
		layoutConstraint.calculatedMinSize = calcMinimumContentSize(context);
		Insets insets = getBorder().thickness.merge(getPadding());
		layoutConstraint.calculatedMinSize.w += insets.l;
		layoutConstraint.calculatedMinSize.w += insets.r;
		layoutConstraint.calculatedMinSize.h += insets.t;
		layoutConstraint.calculatedMinSize.h += insets.b;
		return layoutConstraint.calculatedMinSize;
	}
	
	/**
	 * Calculates minimum size required to show itself. 
	 * Margin, border, padding excluded.
	 * @param context
	 * @return
	 */
	public abstract Dimension calcMinimumContentSize(DocumentContext context);
}
