package com.vasnabilisim.olive.core.document;

import java.util.EnumSet;

import com.vasnabilisim.olive.core.property.AbstractEnumProperty;
import com.vasnabilisim.olive.core.property.AbstractEnumSetProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;

/**
 * @author Menderes Fatih GUVEN
 */
public enum PartType {
	ReportHeader, 
	PageHeader, 
	Body, 
	PageFooter, 
	ReportFooter;
	
	static {
		PropertyFactoryX.getInstance().register("PartType", PartTypeProperty.class);
	}
	

	public static class PartTypeProperty extends AbstractEnumProperty<PartType> {
		private static final long serialVersionUID = -2352590679796359517L;

		public PartTypeProperty() {
			super();
		}

		public PartTypeProperty(String group, String alias, String name, String description, PartType value) {
			super(group, alias, name, description, value);
		}

		public PartTypeProperty(PartTypeProperty source) {
			super(source);
		}

		@Override
		public PartTypeProperty clone() {
			return new PartTypeProperty(this);
		}

		@Override
		public String getId() {
			return "PartType";
		}

		@Override
		public Class<PartType> getValueType() {
			return PartType.class;
		}

	}

	public static class PartTypeSetProperty extends AbstractEnumSetProperty<PartType> {
		private static final long serialVersionUID = -5275639589032449985L;

		public PartTypeSetProperty() {
			super();
		}

		public PartTypeSetProperty(String group, String alias, String name, String description, EnumSet<PartType> value) {
			super(group, alias, name, description, value);
		}

		public PartTypeSetProperty(PartTypeSetProperty source) {
			super(source);
		}

		@Override
		public PartTypeSetProperty clone() {
			return new PartTypeSetProperty(this);
		}

		@Override
		public String getId() {
			return "PartTypeSet";
		}

		@Override
		public Class<PartType> getEnumType() {
			return PartType.class;
		}
	}
}
