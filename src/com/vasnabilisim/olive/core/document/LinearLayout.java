package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.DimensionPolicy;
import com.vasnabilisim.olive.core.geom.Insets;
import com.vasnabilisim.olive.core.property.DimensionPolicyProperty;
import com.vasnabilisim.olive.core.property.FloatProperty;
import com.vasnabilisim.olive.core.property.InsetsProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;
import com.vasnabilisim.olive.core.property.PropertyGroup;

/**
 * Linear container. 
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class LinearLayout extends Layout {
	private static final long serialVersionUID = 2070458886271070345L;

	protected LinearLayout() {
		super();
	}
	
	protected LinearLayout(LinearLayout source) {
		super(source);
	}

	@Override
	protected Dimension calcMinimumSizeOf(DocumentContext context, DesignNode node) {
		LinearLayoutConstraint constraint = (LinearLayoutConstraint) node.getLayoutConstraint();
		constraint.calculatedMinSize = Dimension.Zero.clone();
		Dimension nodeMinSize = null;
		switch (constraint.getWidthPolicy()) {
		case Fixed:	constraint.calculatedMinSize.w = context.toPixels(constraint.getWidth()); break;
		case Fill:	constraint.calculatedMinSize.w = 0.0f; break;
		case Wrap:	constraint.calculatedMinSize.w = (nodeMinSize = node.calcMinimumSize(context)).w; break;
		}
		switch (constraint.getHeightPolicy()) {
		case Fixed:	constraint.calculatedMinSize.h = context.toPixels(constraint.getHeight()); break;
		case Fill:	constraint.calculatedMinSize.h = 0.0f; break;
		case Wrap:	constraint.calculatedMinSize.h = nodeMinSize == null ? node.calcMinimumSize(context).h : nodeMinSize.h; break;
		}
		return constraint.calculatedMinSize;
	}
	
	public abstract static class LinearLayoutConstraint extends LayoutConstraint {
		private static final long serialVersionUID = 2434663290325931766L;

		protected DimensionPolicyProperty widthPolicyProperty;
		protected FloatProperty widthProperty;
		protected DimensionPolicyProperty heightPolicyProperty;
		protected FloatProperty heightProperty;
		protected InsetsProperty marginProperty;

		protected LinearLayoutConstraint() {
			super();
			this.widthPolicyProperty = PropertyFactoryX.createDimensionPolicyProperty("Layout", "Width Policy", "WidthPolicy", "Width Policy", DimensionPolicy.Fixed);
			this.widthProperty = PropertyFactoryX.createFloatProperty("Layout", "Width", "Width", "Width", 0f);
			this.heightPolicyProperty = PropertyFactoryX.createDimensionPolicyProperty("Layout", "Height Policy", "HeightPolicy", "Height Policy", DimensionPolicy.Fixed);
			this.heightProperty = PropertyFactoryX.createFloatProperty("Layout", "Height", "Height", "Height", 0f);
			this.marginProperty = PropertyFactoryX.createInsetsProperty("Layout", "Margin", "Margin", "Margin", Insets.Zero.clone());
			this.propertyGroup.add(this.widthPolicyProperty);
			this.propertyGroup.add(this.widthProperty);
			this.propertyGroup.add(this.heightPolicyProperty);
			this.propertyGroup.add(this.heightProperty);
			this.propertyGroup.add(this.marginProperty);
		}
		
		protected LinearLayoutConstraint(PropertyGroup propertyGroup) {
			super();
			this.widthPolicyProperty = propertyGroup.get("WidthPolicy", DimensionPolicyProperty.class);
			this.widthProperty = propertyGroup.get("WidthFillRatio", FloatProperty.class);
			this.heightPolicyProperty = propertyGroup.get("HeightPolicy", DimensionPolicyProperty.class);
			this.heightProperty = propertyGroup.get("HeightFillRatio", FloatProperty.class);
			this.marginProperty = propertyGroup.get("Margin", InsetsProperty.class);
			this.propertyGroup.add(this.widthPolicyProperty);
			this.propertyGroup.add(this.widthProperty);
			this.propertyGroup.add(this.heightPolicyProperty);
			this.propertyGroup.add(this.heightProperty);
			this.propertyGroup.add(this.marginProperty);
		}
		
		protected LinearLayoutConstraint(LinearLayoutConstraint source) {
			super(source);
			this.widthPolicyProperty = source.widthPolicyProperty.clone();
			this.widthProperty = source.widthProperty.clone();
			this.heightPolicyProperty = source.heightPolicyProperty.clone();
			this.heightProperty = source.heightProperty.clone();
			this.marginProperty = source.marginProperty.clone();
			this.propertyGroup.add(this.widthPolicyProperty);
			this.propertyGroup.add(this.widthProperty);
			this.propertyGroup.add(this.heightPolicyProperty);
			this.propertyGroup.add(this.heightProperty);
			this.propertyGroup.add(this.marginProperty);
		}
		
		@Override
		public abstract LinearLayoutConstraint clone();
		
		public DimensionPolicy getWidthPolicy() {
			return widthPolicyProperty.getValue();
		}
		
		public void setWidthPolicy(DimensionPolicy value) {
			widthPolicyProperty.setValue(value);
		}
		
		public Float getWidth() {
			return widthProperty.getValue();
		}
		
		public void setWidth(Float value) {
			widthProperty.setValue(value);
		}
		
		public DimensionPolicy getHeightPolicy() {
			return heightPolicyProperty.getValue();
		}
		
		public void setHeightPolicy(DimensionPolicy value) {
			heightPolicyProperty.setValue(value);
		}
		
		public Float getHeight() {
			return heightProperty.getValue();
		}
		
		public void setHeight(Float value) {
			heightProperty.setValue(value);
		}
		
		public Insets getMargin() {
			return marginProperty.getValue();
		}
		
		public void setMargin(Insets value) {
			marginProperty.setValue(value);
		}
	}
}
