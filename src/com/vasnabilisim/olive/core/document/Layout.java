package com.vasnabilisim.olive.core.document;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.property.Properties;

/**
 * Base class of all container item types. Containers can contain other design items.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class Layout extends RectangularNode {
	private static final long serialVersionUID = -9187872296971754507L;
	
	protected ArrayList<DesignNode> children;

	public Layout() {
		super();
		children = new ArrayList<>();
	}

	public Layout(Layout source) {
		super(source);
		this.children = new ArrayList<>(source.children.size());
		source.children.forEach(n -> addChild(n.clone()));
	}
	
	@Override
	public abstract Layout clone();
	
	/**
	 * Returns a list of children.
	 * 
	 * @return
	 */
	public List<DesignNode> getChildren() {
		return Collections.unmodifiableList(this.children);
	}

	/**
	 * Adds given child.
	 * 
	 * @param child
	 */
	public void addChild(DesignNode child) {
		if (child.parent != null)
			child.parent.removeChild(child);
		children.add(child);
		child.parent = this;
		child.layoutConstraint = createLayoutConstraint();
		this.layoutConstraint.invalidate();
	}

	/**
	 * Removes given child.
	 * @param child
	 * @return
	 */
	public boolean removeChild(DesignNode child) {
		if(child.parent != this)
			return false;
		children.remove(child);
		child.parent = null;
		child.layoutConstraint = null;
		this.layoutConstraint.invalidate();
		return true;
	}

	/**
	 * Creates a layout constraint compatible with this layout type.
	 * @return
	 */
	protected abstract LayoutConstraint createLayoutConstraint();

	/**
	 * Creates a layout constraint compatible with this layout type from given properties.
	 * @param p
	 * @return
	 */
	protected abstract LayoutConstraint createLayoutConstraint(Properties p);
	
	/**
	 * Recalculate bounds of the layout and its children
	 * @param context
	 */
	public void validateBounds(DocumentContext context) {
	}
	
	/**
	 * Recalculate bounds of sub layouts
	 * @param context
	 */
	public void validateChildrenBounds(DocumentContext context) {
		for(DesignNode child : children) {
			if(child instanceof Layout)
				((Layout)child).validateBounds(context);
		}
	}
	
	/**
	 * Calculates minimum required size of given node in pixels.
	 * @param context
	 * @param node
	 * @return
	 */
	protected abstract Dimension calcMinimumSizeOf(DocumentContext context, DesignNode node);
}
