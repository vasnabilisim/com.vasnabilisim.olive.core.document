package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.geom.Alignment;
import com.vasnabilisim.olive.core.property.AlignmentProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;

/**
 * All label items are of this type.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class Label extends RectangularNode {
	private static final long serialVersionUID = 3624507200867493743L;

	protected AlignmentProperty alignmentProperty;
	
	protected Label() {
		super();
		this.alignmentProperty = PropertyFactoryX.createAlignmentProperty("Content", "Alignment", "Alignment", "Alignment of content", Alignment.CenterLeading);
		this.properties.add(this.alignmentProperty);
	}
	
	protected Label(Label source) {
		super(source);
		this.alignmentProperty = source.alignmentProperty.clone();
		this.properties.add(this.alignmentProperty);
	}
	
	@Override
	public abstract Label clone();
	
	public Alignment getAlignment() {
		return alignmentProperty.getValue();
	}
	
	public void setAlignment(Alignment value) {
		alignmentProperty.setValue(value);
	}
}
