package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.Rectangle;
import com.vasnabilisim.olive.core.property.Properties;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;
import com.vasnabilisim.olive.core.property.PropertyGroup;
import com.vasnabilisim.olive.core.property.RectangleProperty;

/**
 * XY layout. 
 * 
 * @author Menderes Fatih GUVEN
 */
public class XYLayout extends Layout {
	private static final long serialVersionUID = -1015857733447306849L;

	public XYLayout() {
		super();
	}

	public XYLayout(XYLayout source) {
		super(source);
	}
	
	@Override
	public XYLayout clone() {
		return new XYLayout(this);
	}

	@Override
	public Dimension calcMinimumContentSize(DocumentContext context) {
		Dimension size = Dimension.Zero.clone();
		for(DesignNode child : children) {
			XYLayoutConstraint constraint = (XYLayoutConstraint) child.getLayoutConstraint();
			size.w = Math.max(size.w, constraint.getBounds().getX2());
			size.h = Math.max(size.h, constraint.getBounds().getY2());
		}
		return size;
	}
	
	@Override
	protected Dimension calcMinimumSizeOf(DocumentContext context, DesignNode node) {
		XYLayoutConstraint constraint = (XYLayoutConstraint) node.getLayoutConstraint();
		constraint.calculatedMinSize = constraint.getBounds().getDimension();
		return constraint.calculatedMinSize;
	}
	
	@Override
	public void validateBounds(DocumentContext context) {
		for(DesignNode child : children) {
			XYLayoutConstraint constraint = (XYLayoutConstraint) child.getLayoutConstraint();
			constraint.calculatedBounds = constraint.getBounds();
		}
		validateChildrenBounds(context);
	}
	
	@Override
	protected XYLayoutConstraint createLayoutConstraint() {
		return new XYLayoutConstraint();
	}

	@Override
	protected XYLayoutConstraint createLayoutConstraint(Properties p) {
		return new XYLayoutConstraint(p.getGroup("Layout"));
	}
	
	public static class XYLayoutConstraint extends LayoutConstraint {
		private static final long serialVersionUID = 7836402366768622940L;

		//protected PointProperty locationProperty;
		protected RectangleProperty boundsProperty;
		
		protected XYLayoutConstraint() {
			this.boundsProperty = PropertyFactoryX.createRectangleProperty("Layout", "Bounds", "Bounds", "bounds of the element", Rectangle.Zero.clone());
			this.propertyGroup.add(this.boundsProperty);
		}
		
		protected XYLayoutConstraint(PropertyGroup propertyGroup) {
			super();
			this.boundsProperty = propertyGroup.get("Bounds", RectangleProperty.class);
			this.propertyGroup.add(this.boundsProperty);
		}
		
		protected XYLayoutConstraint(XYLayoutConstraint source) {
			super(source);
			this.boundsProperty = source.boundsProperty.clone();
			this.propertyGroup.add(this.boundsProperty);
		}

		@Override
		public XYLayoutConstraint clone() {
			return new XYLayoutConstraint(this);
		}
		
		public Rectangle getBounds() {
			return boundsProperty.getValue();
		}
		
		public void setBounds(Rectangle value) {
			boundsProperty.setValue(value);
		}
	}
}
