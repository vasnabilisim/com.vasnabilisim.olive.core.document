package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.paint.Canvas;

/**
 * Horizontal ruler
 * 
 * @author Menderes Fatih GUVEN
 */
public class HorizontalRuler extends Ruler {
	private static final long serialVersionUID = -1184058621001637870L;

	/**
	 * Height of the ruler.
	 */
	protected float height = RULER_THICKNESS;

	/**
	 * Default constructor.
	 */
	public HorizontalRuler() {
		super();
	}

	/**
	 * Copy constructor.
	 * @param source
	 */
	public HorizontalRuler(HorizontalRuler source) {
		super(source);
		this.height = source.height;
	}

	@Override
	public HorizontalRuler clone() {
		return new HorizontalRuler(this);
	}

	@Override
	public float getWidth() {
		return paper.getOrientedWidth();
	}

	@Override
	public float getHeight() {
		return height;
	}

	/**
	 * Sets the height.
	 * 
	 * @param height
	 */
	public void setHeight(int height) {
		this.height = height;
		fireDocumentModifiedEvent("height");
	}

	/**
	 * Paints the ruler to given graphics device.
	 * 
	 * @param g
	 */
	public void paint(Canvas g) {
		/*
		// Component height.
		float rulerHeight = getHeight();
		// Paper width.
		float paperWidth = getPage().getOrientedWidth();

		// Paint background.
		g.setColor(getBackground());
		g.fillRect(0, 0, paperWidth, rulerHeight);

		// Paper left and right margins.
		float paperLeftMargin = getPage().getLeftMargin();
		float paperRightMargin = getPage().getRightMargin();

		// Paint margins.
		g.setColor(getMarginColor());
		// Left margin.
		g.fillRect(0, 0, paperLeftMargin, rulerHeight);
		// Right margin.
		g.fillRect(paperWidth - paperRightMargin, 0, paperRightMargin,
				rulerHeight);

		// Paint border.
		g.setColor(getDarkBackground());
		g.drawRect(0, 0, paperWidth, rulerHeight);

		// Paint units
		g.setColor(getUnitColor());
		double x = 0.0;
		float pixelCount = PageUnit.Inch.pixelCount();
		if (getPage().getUnit() == PageUnit.Cm)
			pixelCount = PageUnit.Cm.pixelCount();
		while (x <= paperWidth) {
			g.fillRect((int) (x - pixelCount / 2), 7, 1, 6);
			g.fillRect((int) x, 5, 1, 10);
			x += pixelCount;
		}

		// Paint mark.
		g.setPaint(getMarkColor());
		g.fillRect(paperLeftMargin + markStart, 0, markLength, rulerHeight);
		*/
	}
}
