package com.vasnabilisim.olive.core.document;

import java.awt.Cursor;

import com.vasnabilisim.olive.core.paint.Color;
import com.vasnabilisim.olive.core.paint.Font;
import com.vasnabilisim.olive.core.paper.PaperType;

/**
 * @author Menderes Fatih GUVEN
 */
public interface Constants {

	/**
	 * Default ruler thickness.
	 */
	int RULER_THICKNESS = 20;

	/**
	 * Number of pixel between parts.
	 */
	int BLANK_THICKNESS = 10;

	/**
	 * Default part height.
	 */
	int PART_HEIGHT = 150;
	
	/**
	 * Default part background.
	 */
	Color PART_BACKGROUD = Color.White;

	/**
	 * Default part foreground.
	 */
	Color PART_FOREGROUD = new Color(.5f, 1f, .5f, .5f);

	/**
	 * Default report background color.
	 */
	Color REPORT_BACKGROUND = new Color(.5f, .5f, 1f);
	
	/**
	 * Default report divider color.
	 */
	Color REPORT_DIVIDER_COLOR = new Color(.375f, .375f, .75f);
	
	/**
	 * Default margin color.
	 */
	Color MARGIN_COLOR = new Color(.9375f, .9375f, .9375f);
	
	/**
	 * Default ruler unit color.
	 */
	Color RULER_UNIT_COLOR = Color.Blue;
	
	/**
	 * Default ruler unit color.
	 */
	Color RULER_MARK_COLOR = new Color(0f, 0f, 1f, .15625f);
	
	/**
	 * Default ruler background color.
	 */
	Color RULER_BACKGROUND = Color.White;
	
	/**
	 * Default ruler dark background color.
	 */
	Color RULER_DARK_BACKGROUND = new Color(0f, 0f, .5f);
	
	/**
	 * Default selection color.
	 */
	Color SELECTION_COLOR = new Color(.75f, .75f, 1f, .5f);

	/**
	 * Default selection border color.
	 */
	Color SELECTION_BORDER_COLOR = new Color(0f, 0f, .5f);

	/**
	 * Default border color.
	 */
	Color BORDER_COLOR = new Color(.5f, .5f, 1f);
	
	/**
	 * Default border.
	Border BORDER = BorderFactory.createLineBorder(BORDER_COLOR);
	 */
	
	/**
	 * Default font family.
	 */
	String COMPONENT_FONT_FAMILY = "Dialog";
	
	/**
	 * Default font.
	 */
	Font COMPONENT_FONT = new Font(COMPONENT_FONT_FAMILY, 10f);

	/**
	 * Default page type.
	 */
	PaperType PAGE_TYPE = PaperType.Default;
	
	/**
	 * 0, 0, 0, 0
	Insets ZERO_INSETS = new Insets(0, 0, 0, 0);
	 */

	/**
	 * Resize point size.
	 */
	int RESIZE_POINT_SIZE = 5;
	
	/**
	 * Resize north west.
	 */
	int RESIZE_NW	= Cursor.NW_RESIZE_CURSOR;
	
	/**
	 * Resize north.
	 */
	int RESIZE_N	= Cursor.N_RESIZE_CURSOR;
	
	/**
	 * Resize north east.
	 */
	int RESIZE_NE	= Cursor.NE_RESIZE_CURSOR;
	
	/**
	 * Resize east.
	 */
	int RESIZE_E	= Cursor.E_RESIZE_CURSOR;
	
	/**
	 * Resize south east.
	 */
	int RESIZE_SE	= Cursor.SE_RESIZE_CURSOR;
	
	/**
	 * Resize south.
	 */
	int RESIZE_S	= Cursor.S_RESIZE_CURSOR;
	
	/**
	 * Resize south west.
	 */
	int RESIZE_SW	= Cursor.SW_RESIZE_CURSOR;
	
	/**
	 * Resize west.
	 */
	int RESIZE_W	= Cursor.W_RESIZE_CURSOR;
	
	/**
	 * Move.
	 */
	int MOVE		= Cursor.MOVE_CURSOR;
}
