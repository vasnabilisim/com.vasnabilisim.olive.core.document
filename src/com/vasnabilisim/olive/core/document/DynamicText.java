package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.model.Format;
import com.vasnabilisim.olive.core.paint.Color;
import com.vasnabilisim.olive.core.paint.Font;
import com.vasnabilisim.olive.core.property.ColorProperty;
import com.vasnabilisim.olive.core.property.FontProperty;
import com.vasnabilisim.olive.core.property.FormatProperty;
import com.vasnabilisim.olive.core.property.IntegerProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;

/**
 * Static text.
 * @author Menderes Fatih GUVEN
 */
public class DynamicText extends DynamicLabel {
	private static final long serialVersionUID = -1728133874258598456L;

	protected ColorProperty foregroundProperty;
	protected FormatProperty formatProperty;
	protected FontProperty fontProperty;
	protected IntegerProperty maximumLinesProperty;
	
	/**
	 * Default constructor.
	 */
	public DynamicText() {
		super();
		this.formatProperty = PropertyFactoryX.createFormatProperty("Field", "Format", "Format", "Format of the field value", null);
		this.foregroundProperty = PropertyFactoryX.createColorProperty("Text", "Color", "Foreground", "Text color", Color.Black);
		this.fontProperty = PropertyFactoryX.createFontProperty("Text", "Font", "Font", "Font of the text", Font.Default);
		this.maximumLinesProperty = PropertyFactoryX.createIntegerProperty("Text", "Maximum Lines", "MaximumLines", "Maximum number of lines", 1);
		this.properties.add(this.foregroundProperty);
		this.properties.add(this.formatProperty);
		this.properties.add(this.fontProperty);
		this.properties.add(this.maximumLinesProperty);
	}

	/**
	 * Copy constructor.
	 * @param source
	 */
	public DynamicText(DynamicText source) {
		super(source);
		this.formatProperty = source.formatProperty.clone();
		this.foregroundProperty = source.foregroundProperty.clone();
		this.fontProperty = source.fontProperty.clone();
		this.maximumLinesProperty = source.maximumLinesProperty.clone();
		this.properties.add(this.foregroundProperty);
		this.properties.add(this.formatProperty);
		this.properties.add(this.fontProperty);
		this.properties.add(this.maximumLinesProperty);
	}

	/**
	 * @see com.vasnabilisim.olive.core.document.StaticLabel#cloneObject()
	 */
	@Override
	public DynamicText clone() {
		return new DynamicText(this);
	}
	
	public Format getFormat() {
		return formatProperty.getValue();
	}
	
	public void setFormat(Format value) {
		formatProperty.setValue(value);
	}

	public Color getForeground() {
		return foregroundProperty.getValue();
	}
	
	public void setForeground(Color value) {
		foregroundProperty.setValue(value);
	}
	
	public Font getFont() {
		return fontProperty.getValue();
	}
	
	public void setFont(Font value) {
		fontProperty.setValue(value);
	}
	
	public Integer getMaximumLines() {
		return maximumLinesProperty.getValue();
	}
	
	public void setMaximumLines(Integer value) {
		maximumLinesProperty.setValue(value);
	}
	
	@Override
	public Dimension calcMinimumContentSize(DocumentContext context) {
		return context.toLength(getFieldStatement().getStatement().getText(), getFont(), getMaximumLines());
	}
}
