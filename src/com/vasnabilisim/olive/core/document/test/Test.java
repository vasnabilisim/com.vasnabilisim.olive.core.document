package com.vasnabilisim.olive.core.document.test;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.document.Document;
import com.vasnabilisim.olive.core.document.DocumentIO;

public class Test {

	public Test() {
	}

	public static void main(String[] args) {
		Document document;
		try {
			document = (Document)DocumentIO.read("report-design/trial01.xml");
		} catch (BaseException e) {
			e.printStackTrace();
			System.exit(-1);
			return;
		}
		System.out.println(document);
	}

}
