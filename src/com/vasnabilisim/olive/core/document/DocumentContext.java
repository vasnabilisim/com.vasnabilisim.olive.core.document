package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.document.metrics.Metrics;
import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.HorizontalAlignment;
import com.vasnabilisim.olive.core.geom.Rectangle;
import com.vasnabilisim.olive.core.paint.Font;
import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.paper.PaperUnit;

public class DocumentContext {

	Document document;
	Part part;
	Metrics metrics;
	boolean rtl;
	
	public DocumentContext(Document document, Part part, Metrics metrics, boolean rtl) {
		this.document = document;
		this.part = part;
		this.metrics = metrics;
		this.rtl = rtl;
	}
	
	public Document getDocument() {
		return document;
	}
	
	public Paper getPaper() {
		return document.getPaper();
	}
	
	public PaperUnit getUnit() {
		return document.getPaper().getUnit();
	}
	
	public Part getPart() {
		return part;
	}
	
	public Metrics getMetrics() {
		return metrics;
	}
	
	public boolean isRtl() {
		return rtl;
	}
	
	public HorizontalAlignment decide(HorizontalAlignment alignment) {
		switch (alignment) {
		case Left:		return HorizontalAlignment.Left;
		case Leading:	return rtl ? HorizontalAlignment.Right : HorizontalAlignment.Left;
		case Right:		return HorizontalAlignment.Right;
		case Trailing:	return rtl ? HorizontalAlignment.Left : HorizontalAlignment.Right;
		default:		return HorizontalAlignment.Center;
		}
	}
	
	public float toPixels(float value) {
		return metrics.toPixels(value, document.getPaper().getUnit());
	}
	
	public Dimension toPixels(Dimension value) {
		return metrics.toPixels(value, document.getPaper().getUnit());
	}
	
	public Rectangle toPixels(Rectangle value) {
		return metrics.toPixels(value, document.getPaper().getUnit());
	}
	
	public Dimension toPixels(String text, Font font, Integer maximumLines) {
		return metrics.toPixels(text, font, maximumLines);
	}
	
	public float toLength(float value) {
		return metrics.toLength(value, document.getPaper().getUnit());
	}
	
	public Dimension toLength(Dimension value) {
		return metrics.toLength(value, document.getPaper().getUnit());
	}
	
	public Rectangle toLength(Rectangle value) {
		return metrics.toLength(value, document.getPaper().getUnit());
	}
	
	public Dimension toLength(String text, Font font, Integer maximumLines) {
		return metrics.toLength(getUnit(), text, font, maximumLines);
	}
}
