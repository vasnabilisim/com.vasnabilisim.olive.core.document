package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.paint.Color;
import com.vasnabilisim.olive.core.paint.Font;
import com.vasnabilisim.olive.core.property.ColorProperty;
import com.vasnabilisim.olive.core.property.FontProperty;
import com.vasnabilisim.olive.core.property.IntegerProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;
import com.vasnabilisim.olive.core.property.StringProperty;

/**
 * Static text.
 * @author Menderes Fatih GUVEN
 */
public class StaticText extends StaticLabel {
	private static final long serialVersionUID = -5829143865546651809L;

	protected ColorProperty foregroundProperty;
	protected StringProperty textProperty;
	protected FontProperty fontProperty;
	protected IntegerProperty maximumLinesProperty;
	
	/**
	 * Default constructor.
	 */
	public StaticText() {
		super();
		this.foregroundProperty = PropertyFactoryX.createColorProperty("Text", "Color", "Foreground", "Text color", Color.Black);
		this.textProperty = PropertyFactoryX.createStringProperty("Text", "Text", "Text", "Text to write", null);
		this.fontProperty = PropertyFactoryX.createFontProperty("Text", "Font", "Font", "Font of the text", Font.Default);
		this.maximumLinesProperty = PropertyFactoryX.createIntegerProperty("Text", "Maximum Lines", "MaximumLines", "Maximum number of lines", 1);
		this.properties.add(this.foregroundProperty);
		this.properties.add(this.textProperty);
		this.properties.add(this.fontProperty);
		this.properties.add(this.maximumLinesProperty);
	}

	/**
	 * Copy constructor.
	 * @param source
	 */
	public StaticText(StaticText source) {
		super(source);
		this.foregroundProperty = source.foregroundProperty.clone();
		this.textProperty = source.textProperty.clone();
		this.fontProperty = source.fontProperty.clone();
		this.maximumLinesProperty = source.maximumLinesProperty.clone();
		this.properties.add(this.foregroundProperty);
		this.properties.add(this.textProperty);
		this.properties.add(this.fontProperty);
		this.properties.add(this.maximumLinesProperty);
	}

	@Override
	public StaticText clone() {
		return new StaticText(this);
	}

	public Color getForeground() {
		return foregroundProperty.getValue();
	}
	
	public void setForeground(Color value) {
		foregroundProperty.setValue(value);
	}
	
	public String getText() {
		return textProperty.getValue();
	}
	
	public void setText(String value) {
		textProperty.setValue(value);
	}
	
	public Font getFont() {
		return fontProperty.getValue();
	}
	
	public void setFont(Font value) {
		fontProperty.setValue(value);
	}
	
	public Integer getMaximumLines() {
		return maximumLinesProperty.getValue();
	}
	
	public void setMaximumLines(Integer value) {
		maximumLinesProperty.setValue(value);
	}
	
	@Override
	public Dimension calcMinimumContentSize(DocumentContext context) {
		return context.toLength(getText(), getFont(), getMaximumLines());
	}
}
