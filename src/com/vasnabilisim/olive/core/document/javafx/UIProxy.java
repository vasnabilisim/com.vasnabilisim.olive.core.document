package com.vasnabilisim.olive.core.document.javafx;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.paint.Font;
import com.vasnabilisim.olive.core.paint.Paint;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class UIProxy {

	private static UIProxy Instance = new JavaFxUIProxy();
	
	public static UIProxy getInstance() {
		return Instance;
	}
	
	public static void setInstance(UIProxy instance) {
		Instance = instance;
	}
	
	protected UIProxy() {
	}

	public abstract Dimension calcStringSize(String value, int maxLines, Font font, Paint<?> paint);
	
}
