package com.vasnabilisim.olive.core.document.javafx;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.paint.Font;
import com.vasnabilisim.olive.core.paint.Paint;

/**
 * @author Menderes Fatih GUVEN
 */
public class JavaFxUIProxy extends UIProxy {

	
	public JavaFxUIProxy() {
	}

	/**
	 * @see com.vasnabilisim.olive.core.document.javafx.UIProxy#calcStringSize(java.lang.String, int, com.vasnabilisim.olive.core.paint.Font, com.vasnabilisim.olive.core.paint.Paint)
	 */
	@Override
	public Dimension calcStringSize(String value, int maxLines, Font font, Paint<?> paint) {
		//TODO maxLines?
		Dimension dimension = Dimension.Zero.clone();
		if(value == null)
			return dimension;
		/*
		Text text = new Text(value);
		StringBuilder style = new StringBuilder(50);
		style.append("-fx-font:").append(font.getName()).append(";");
		style.append("-fx-font-size:").append(font.getSize()).append(";");
		if(!font.getStyles().isEmpty()) {
			style.append("-fx-font-weight:");
			boolean comma = false;
			for(FontStyle fontStyle : font.getStyles()) {
				if(comma) style.append(',');
				switch (fontStyle) {
				case Italic		: style.append("italic"); break;
				case Bold		: style.append("bold"); break;
				case Underline	: style.append("underline"); break;
				default			: break;
				}
				comma = true;
			}
			style.append(";");
		}
		if(paint != null && paint instanceof Color) {
			Color color = (Color) paint;
			style.append("-fx-font-fill:rgba(").append((int)(color.r*255f)).append(",").append((int)(color.g*255f)).append(",").append((int)(color.b*255f)).append(",").append(color.o).append(");");
		}
		text.setStyle(style.toString());
		
		Bounds layoutBounds = text.getLayoutBounds();
		dimension.w = (float) layoutBounds.getWidth();
		dimension.h = (float) layoutBounds.getHeight();
		*/
		return dimension;
	}
}
