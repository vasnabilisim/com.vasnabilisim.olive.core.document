package com.vasnabilisim.olive.core.document;

import java.util.EnumSet;

import com.vasnabilisim.olive.core.property.FloatProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;

/**
 * @author Menderes Fatih GUVEN
 */
public class Part extends Node {
	private static final long serialVersionUID = -7332858478164210470L;

	protected Document document;
	protected Layout layout;
	
	protected PartType.PartTypeSetProperty typeSetProperty;
	protected FloatProperty heightProperty;
	
	public Part() {
		super();
		this.typeSetProperty = new PartType.PartTypeSetProperty("Types", "Types", "Types", "Where part exists within page", EnumSet.of(PartType.Body));
		this.heightProperty = PropertyFactoryX.createFloatProperty("Height", "Height", "Height", "Height of report part", 0f);
		this.properties.add(this.typeSetProperty);
		this.properties.add(this.heightProperty);
	}

	public Part(Part source) {
		super(source);
		this.layout = source.layout == null ? null : source.layout.clone();
		this.typeSetProperty = source.typeSetProperty.clone();
		this.heightProperty = source.heightProperty.clone();
		this.properties.add(this.typeSetProperty);
		this.properties.add(this.heightProperty);
	}

	@Override
	public Part clone() {
		return new Part(this);
	}
	
	public Document getDocument() {
		return document;
	}
	
	public void setDocument(Document document) {
		this.document = document;
	}

	public Layout getLayout() {
		return layout;
	}
	
	public void setLayout(Layout layout) {
		this.layout = layout;
	}
	
	@SuppressWarnings("unchecked")
	public EnumSet<PartType> getTypes() {
		return typeSetProperty.getValue();
	}
	
	public void setType(EnumSet<PartType> value) {
		typeSetProperty.setValue(value);
	}
	/*
	public float getHeight() {
		return layout == null ? 0 : layout.getHeight();
	}
	
	public void setHeight(float value) {
		if(layout != null)
			layout.setHeight(value);
	}
	*/
	
	/*
	@Override
	public void addChild(DesignNode<?> child) {
		children.clear();
		if(!(child instanceof Layout))
			return;
		super.addChild(child);
	}
	
	@Override
	public boolean removeChild(DesignNode<?> child) {
		if(!(child instanceof Layout))
			return false;
		return super.removeChild(child);
	}
	
	@Override
	protected void calcRectanges() {
		Layout<?> layout = getLayout();
		if(layout == null)
			return;
		Page page = getDocument().getProperties().getValue("Paper", "Page", Page.class);
		layout.bounds.x = 0f;
		layout.bounds.y = 0f;
		layout.bounds.w = page.getOrientedImageableWidth();
		layout.bounds.h = getProperties().getValue("Height", "Height", Float.class, 0f);
	}

	@Override
	protected PartConstraint createLayoutConstraint() {
		return Constraint;
	}

	@Override
	protected PartConstraint createLayoutConstraint(Properties p) {
		return Constraint;
	}

	static PartConstraint Constraint = new PartConstraint();
	public static class PartConstraint extends LayoutConstraint<PartConstraint> {
		private static final long serialVersionUID = -1759288037825630576L;
		protected PartConstraint() {}
		@Override
		public PartConstraint cloneObject() {return this;}
	}
	*/
}
