package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.model.statement.FieldStatement;
import com.vasnabilisim.olive.core.property.FieldStatementProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;

/**
 * All static labels are of this type.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class DynamicLabel extends Label {
	private static final long serialVersionUID = 1157130914285106095L;
	
	protected FieldStatementProperty fieldStatementProperty;
	
	/**
	 * Default constructor.
	 */
	protected DynamicLabel() {
		super();
		this.fieldStatementProperty = PropertyFactoryX.createFieldStatementProperty("Field", "Field", "Statement", "Data to be shown in this element", FieldStatement.Empty);
		this.properties.add(this.fieldStatementProperty);
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	protected DynamicLabel(DynamicLabel source) {
		super(source);
		this.fieldStatementProperty = source.fieldStatementProperty.clone();
		this.properties.add(this.fieldStatementProperty);
	}
	
	@Override
	public abstract DynamicLabel clone();
	
	public FieldStatement getFieldStatement() {
		return fieldStatementProperty.getValue();
	}
	
	public void setFieldStatement(FieldStatement value) {
		fieldStatementProperty.setValue(value);
	}
}
