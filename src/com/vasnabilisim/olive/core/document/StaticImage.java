package com.vasnabilisim.olive.core.document;

import java.awt.image.BufferedImage;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.paint.ImagePaintPolicy;
import com.vasnabilisim.olive.core.property.ImagePaintPolicyProperty;
import com.vasnabilisim.olive.core.property.ImageProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;

/**
 * Static text.
 * @author Menderes Fatih GUVEN
 */
public class StaticImage extends StaticLabel {
	private static final long serialVersionUID = -5829143865546651809L;

	protected ImageProperty imageProperty;
	protected ImagePaintPolicyProperty paintPolicyProperty;
	
	/**
	 * Default constructor.
	 */
	public StaticImage() {
		super();
		this.imageProperty = PropertyFactoryX.createImageProperty("Content", "Image", "Image", "Image to paint", null);
		this.paintPolicyProperty = PropertyFactoryX.createImagePaintPolicyProperty("Content", "Paint Policy", "PaintPolicy", "Image paint policy", ImagePaintPolicy.Single);
		this.properties.add(this.imageProperty);
		this.properties.add(this.paintPolicyProperty);
	}

	/**
	 * Copy constructor.
	 * @param source
	 */
	public StaticImage(StaticImage source) {
		super(source);
		this.imageProperty = source.imageProperty.clone();
		this.paintPolicyProperty = source.paintPolicyProperty.clone();
		this.properties.add(this.imageProperty);
		this.properties.add(this.paintPolicyProperty);
	}

	/**
	 * @see com.vasnabilisim.olive.core.document.StaticLabel#cloneObject()
	 */
	@Override
	public StaticImage clone() {
		return new StaticImage(this);
	}

	public BufferedImage getImage() {
		return imageProperty.getValue();
	}
	
	public void setImage(BufferedImage value) {
		imageProperty.setValue(value);
	}
	
	public ImagePaintPolicy getPaintPolicy() {
		return paintPolicyProperty.getValue();
	}
	
	public void setPaintPolicy(ImagePaintPolicy value) {
		paintPolicyProperty.setValue(value);
	}

	@Override
	public Dimension calcMinimumContentSize(DocumentContext context) {
		BufferedImage image = getImage();
		return image == null ? Dimension.Zero.clone() : new Dimension(context.toPixels(image.getWidth()), context.toPixels(image.getHeight()));
	}
}
