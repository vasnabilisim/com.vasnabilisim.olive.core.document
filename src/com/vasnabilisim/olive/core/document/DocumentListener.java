package com.vasnabilisim.olive.core.document;

import java.util.EventListener;

/**
 * Item listener.
 * 
 * @author Menderes Fatih GUVEN
 * 
 */
public interface DocumentListener extends EventListener {

	/**
	 * Fired when a component modified.
	 * 
	 * @param e
	 */
	void documentModified(DocumentEvent e);
}
