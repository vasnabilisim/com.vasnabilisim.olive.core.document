package com.vasnabilisim.olive.core.document.metrics;

/**
 * @author Menderes Fatih GUVEN
 */
public interface FontMetrics {
	
	/**
	 * Returns height of the underlying font.
	 * Returns number or pixels.
	 * @return
	 */
	float height();

	/**
	 * Returns the width of the given text painted with underlying font. 
	 * Returns number of pixels.
	 * @param text
	 * @return
	 */
	float width(String text);
}
