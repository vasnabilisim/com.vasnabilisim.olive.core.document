package com.vasnabilisim.olive.core.document.metrics.awt;

import java.awt.font.TextAttribute;
import java.util.Map;
import java.util.TreeMap;

import com.vasnabilisim.olive.core.document.metrics.FontMetrics;
import com.vasnabilisim.olive.core.paint.FontStyle;

/**
 * @author Menderes Fatih GUVEN
 */
public class AwtFontMetrics implements FontMetrics {

	protected java.awt.Font awtFont;
	
	public AwtFontMetrics(com.vasnabilisim.olive.core.paint.Font font) {
		int awtStyle = 0;
		Map<TextAttribute, Object> fontAttributes = new TreeMap<TextAttribute, Object>();
		for(FontStyle style : font.getStyles()) {
			switch (style) {
			case Bold		: awtStyle |= java.awt.Font.BOLD; break;
			case Italic		: awtStyle |= java.awt.Font.ITALIC; break;
			case Underline	: fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON); break;
			case Overline	: fontAttributes.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON); break;
			default		: break;
			}
		}
		java.awt.Font awtFont = new java.awt.Font(font.getName(), awtStyle, Math.round(font.getSize()));
		if(!fontAttributes.isEmpty())
			awtFont = awtFont.deriveFont(fontAttributes);
	}

	/**
	 * @see com.vasnabilisim.olive.core.document.metrics.FontMetrics#height()
	 */
	@Override
	public float height() {
		return awtFont.getLineMetrics("��", null).getHeight();
	}

	/**
	 * @see com.vasnabilisim.olive.core.document.metrics.FontMetrics#width(java.lang.String)
	 */
	@Override
	public float width(String text) {
		return (float)awtFont.getStringBounds(text, null).getWidth();
	}

}
