package com.vasnabilisim.olive.core.document.metrics.awt;

import com.vasnabilisim.olive.core.document.metrics.FontMetrics;
import com.vasnabilisim.olive.core.document.metrics.Metrics;
import com.vasnabilisim.olive.core.paint.Font;
import com.vasnabilisim.olive.core.paper.PaperUnit;

/**
 * @author Menderes Fatih GUVEN
 */
public class AwtMetrics extends Metrics {

	float dpi = java.awt.Toolkit.getDefaultToolkit().getScreenResolution();
	
	public AwtMetrics() {
	}

	/**
	 * @see com.vasnabilisim.olive.core.document.metrics.Metrics#createFontMetrics(com.vasnabilisim.olive.core.paint.Font)
	 */
	@Override
	public FontMetrics createFontMetrics(Font font) {
		return new AwtFontMetrics(font);
	}

	/**
	 * @see com.vasnabilisim.olive.core.document.metrics.Metrics#toPixels(float, com.vasnabilisim.olive.core.paper.PaperUnit)
	 */
	@Override
	public float toPixels(float value, PaperUnit unit) {
		switch (unit) {
		case inch	: return value * dpi;
		case cm		: 
		default		: return value / INCH_CM * dpi;
		}
	}

	/**
	 * @see com.vasnabilisim.olive.core.document.metrics.Metrics#toLength(float, com.vasnabilisim.olive.core.paper.PaperUnit)
	 */
	@Override
	public float toLength(float pixels, PaperUnit unit) {
		switch (unit) {
		case inch	: return pixels / dpi;
		case cm		: 
		default		: return pixels / dpi * INCH_CM;
		}
	}

}
