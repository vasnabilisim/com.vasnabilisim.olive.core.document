package com.vasnabilisim.olive.core.document.metrics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.Rectangle;
import com.vasnabilisim.olive.core.paint.Font;
import com.vasnabilisim.olive.core.paper.PaperUnit;
import com.vasnabilisim.util.Pair;
import com.vasnabilisim.util.StringUtil;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class Metrics {

	/*
	private static Metrics instance = new AwtMetrics();
	
	public static Metrics getInstance() {
		return instance;
	}
	
	public static void setInstance(Metrics instance) {
		Metrics.instance = instance;
	}
	*/
	
	/**
	 * Creates and returns a font metrics for given font.
	 * @param font
	 * @return
	 */
	public abstract FontMetrics createFontMetrics(Font font);
	
	/**
	 * Converts given length to number of pixels.
	 * @param value
	 * @param unit
	 * @return
	 */
	public abstract float toPixels(float value, PaperUnit unit);
	
	/**
	 * Converts given length dimension to pixel dimension of given unit type.
	 * @param lengthDimension
	 * @param unit
	 * @return
	 */
	public Dimension toPixels(Dimension lengthDimension, PaperUnit unit) {
		Dimension dimension = Dimension.Zero.clone();
		dimension.w = toPixels(lengthDimension.w, unit);
		dimension.h = toPixels(lengthDimension.h, unit);
		return dimension;
	}
	
	/**
	 * Converts given length rectangle to pixel rectangle of given unit type.
	 * @param lengthRectangle
	 * @param unit
	 * @return
	 */
	public Rectangle toPixels(Rectangle lengthRectangle, PaperUnit unit) {
		Rectangle rectangle = Rectangle.Zero.clone();
		rectangle.x = toPixels(lengthRectangle.x, unit);
		rectangle.y = toPixels(lengthRectangle.y, unit);
		rectangle.w = toPixels(lengthRectangle.w, unit);
		rectangle.h = toPixels(lengthRectangle.h, unit);
		return rectangle;
	}
	
	/**
	 * Converts given number of pixels to length of given unit type.
	 * @param pixels
	 * @param unit
	 * @return
	 */
	public abstract float toLength(float pixels, PaperUnit unit);
	
	/**
	 * Converts given pixel dimension to length dimension of given unit type.
	 * @param pixelDimension
	 * @param unit
	 * @return
	 */
	public Dimension toLength(Dimension pixelDimension, PaperUnit unit) {
		Dimension dimension = Dimension.Zero.clone();
		dimension.w = toLength(pixelDimension.w, unit);
		dimension.h = toLength(pixelDimension.h, unit);
		return dimension;
	}
	
	/**
	 * Converts given pixel rectangle to length rectangle of given unit type.
	 * @param pixelRectangle
	 * @param unit
	 * @return
	 */
	public Rectangle toLength(Rectangle pixelRectangle, PaperUnit unit) {
		Rectangle rectangle = Rectangle.Zero.clone();
		rectangle.x = toLength(pixelRectangle.x, unit);
		rectangle.y = toLength(pixelRectangle.y, unit);
		rectangle.w = toLength(pixelRectangle.w, unit);
		rectangle.h = toLength(pixelRectangle.h, unit);
		return rectangle;
	}

	/**
	 * Calculates minimum required pixel size of the given text using given font and line numbers.
	 * @param text
	 * @param font
	 * @param maximumLines
	 * @return
	 */
	public Dimension toPixels(String text, Font font, Integer maximumLines) {
		FontMetrics fontMetrics = createFontMetrics(font);
		List<Pair<String, Float>> lines = toLines(fontMetrics, text, 0f);

		Dimension size = Dimension.Zero.clone();
		size.h =  fontMetrics.height() * (lines.size() < maximumLines ? lines.size() : maximumLines);
		for(Pair<String, Float> line : lines)
			size.w = line.getSecond() > size.w ? line.getSecond() : size.w;
		return size;
	}

	/**
	 * Calculates minimum required size of the given text using given font and line numbers.
	 * @param text
	 * @param font
	 * @param maximumLines
	 * @return
	 */
	public Dimension toLength(PaperUnit unit, String text, Font font, Integer maximumLines) {
		return toLength(toPixels(text, font, maximumLines), unit);
	}

	/**
	 * Splits given text from line breaks. 
	 * Also word wraps splitted texts according to given maxTextWidth. 
	 * Text lengths calculated with given fontMetrics. 
	 * If maxTextWidth is non positive, only splits from line separators.
	 * @param fontMetrics
	 * @param text
	 * @param maxTextWidth
	 * @return
	 */
	public List<Pair<String, Float>> toLines(FontMetrics fontMetrics, String text, float maxTextWidth) {
		if(StringUtil.isEmpty(text))
			Collections.emptyList();
		ArrayList<Pair<String, Float>> lineList;
		String[] lines = text.split("\\r?\\n");
		if(maxTextWidth <= 0f) {
			lineList = new ArrayList<>(lines.length);
			for(String line : lines)
				lineList.add(new Pair<String, Float>(line, fontMetrics.width(line)));
			return lineList;
		}
		lineList = new ArrayList<>();
		for(String line : lines) {
			StringBuilder builder = new StringBuilder(line.length());
			StringTokenizer tokenizer = new StringTokenizer(line, " \t", true);
			String token;
			float lineWidth = 0f;
			float tokenWidth = 0f;
			while(tokenizer.hasMoreTokens()) {
				token = tokenizer.nextToken();
				if(builder.length() == 0 && (" ".equals(token) || "\t".equals(token)) )
					continue;
				tokenWidth = fontMetrics.width(token);
				if(lineWidth + tokenWidth > maxTextWidth) {
					lineList.add(new Pair<>(builder.toString(), lineWidth));
					lineWidth = 0f;
					builder = new StringBuilder(line.length());
				}
				builder.append(token);
				lineWidth += tokenWidth;
			}//while
			if(builder.length() >= 0)
				lineList.add(new Pair<>(builder.toString(), lineWidth));
		}//for
		return lineList;
	}

	/**
	 * Cm value of one inch.
	 */
	public static float INCH_CM	= 2.54f;

	/**
	 * inch value of one cm.
	 */
	public static float CM_INCH	= 1f / INCH_CM; //0.393701f;
}
