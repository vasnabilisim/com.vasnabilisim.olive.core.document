package com.vasnabilisim.olive.core.document;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.property.PaperProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;

/**
 * @author Menderes Fatih GUVEN
 */
public class Document extends Node {
	private static final long serialVersionUID = -4215656620304930571L;

	protected List<Part> parts;
	
	protected PaperProperty paperProperty;
	
	public Document() {
		super();
		this.paperProperty = PropertyFactoryX.createPaperProperty("Paper", "Paper", "Paper", "Paper for overall design", Paper.Default.clone());
		this.properties.add(this.paperProperty);
		this.parts = new ArrayList<>();
	}

	public Document(Document source) {
		super(source);
		this.paperProperty = source.paperProperty.clone();
		this.properties.add(this.paperProperty);
		this.parts = new ArrayList<>(source.parts.size());
		for(Part part : source.parts)
			addPart(part.clone());
	}

	@Override
	public Document clone() {
		return new Document(this);
	}
	
	public List<Part> getParts() {
		return Collections.unmodifiableList(parts);
	}
	
	public void addPart(Part part) {
		parts.add(part);
		part.setDocument(this);
	}
	
	public void removePart(Part part) {
		if(parts.remove(part))
			part.setDocument(null);
	}
	
	public Paper getPaper() {
		return paperProperty.getValue();
	}
	
	public void setPaper(Paper value) {
		paperProperty.setValue(value);
	}

	/*
	@Override
	public void addChild(DesignNode<?> child) {
		if(!(child instanceof Part))
			return;
		super.addChild(child);
	}
	
	@Override
	public boolean removeChild(DesignNode<?> child) {
		if(!(child instanceof Part))
			return false;
		return super.removeChild(child);
	}
	
	@Override
	protected void calcRectanges() {
		for(Part part : getParts()) {
			part.calcRectanges();
		}
	}

	@Override
	protected LayoutConstraint<?> createLayoutConstraint() {
		return Constraint;
	}

	@Override
	protected LayoutConstraint<?> createLayoutConstraint(Properties p) {
		return Constraint;
	}

	static DocumentConstraint Constraint = new DocumentConstraint();
	public static class DocumentConstraint extends LayoutConstraint<DocumentConstraint> {
		private static final long serialVersionUID = -2009464293412188836L;
		protected DocumentConstraint() {}
		@Override
		public DocumentConstraint cloneObject() {return this;}
	}
	*/
}
