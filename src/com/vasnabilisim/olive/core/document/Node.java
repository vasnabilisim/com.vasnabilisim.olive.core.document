package com.vasnabilisim.olive.core.document;

import java.io.Serializable;

import javax.swing.event.EventListenerList;

import com.vasnabilisim.olive.core.property.Properties;

/**
 * Base class of all nodes.
 * @author Menderes Fatih GUVEN
 */
public abstract 
class Node implements Serializable, java.lang.Cloneable {
	private static final long serialVersionUID = 8710800350358779639L;

	protected Properties properties;
	
	protected EventListenerList listenerList = new EventListenerList();

	protected Node() {
		properties = new Properties();
	}

	protected Node(Node source) {
		properties = new Properties();
		//this.properties = source.properties.cloneObject();
	}
	
	@Override
	public abstract Node clone();

	
	public Properties getProperties() {
		return properties.cloneObject();
	}
	
	public void setProperties(Properties properties) {
		properties.addAll(properties);
	}

	/**
	 * Adds document listener.
	 * @param l
	 */
	public void addDocumentListener(DocumentListener l) {
		listenerList.add(DocumentListener.class, l);
	}

	/**
	 * Removes document listener.
	 * @param l
	 */
	public void removeDocumentListener(DocumentListener l) {
		listenerList.remove(DocumentListener.class, l);
	}

	/**
	 * Fire document modified event for given property.
	 * @param property
	 */
	protected void fireDocumentModifiedEvent(String property) {
		DocumentEvent e = new DocumentEvent(this, property);
		for(DocumentListener listener : listenerList.getListeners(DocumentListener.class))
			listener.documentModified(e);
	}
	
}
