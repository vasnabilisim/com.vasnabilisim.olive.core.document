package com.vasnabilisim.olive.core.document;


/**
 * All static labels are of this type.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class StaticLabel extends Label {
	private static final long serialVersionUID = -3303019409157556327L;

	/**
	 * Default constructor.
	 */
	protected StaticLabel() {
		super();
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	protected StaticLabel(StaticLabel source) {
		super(source);
	}
	
	@Override
	public abstract StaticLabel clone();
}
