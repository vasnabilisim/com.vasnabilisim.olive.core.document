package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.DimensionPolicy;
import com.vasnabilisim.olive.core.geom.HorizontalAlignment;
import com.vasnabilisim.olive.core.geom.Insets;
import com.vasnabilisim.olive.core.geom.Rectangle;
import com.vasnabilisim.olive.core.geom.VerticalAlignment;
import com.vasnabilisim.olive.core.property.HorizontalAlignmentProperty;
import com.vasnabilisim.olive.core.property.Properties;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;
import com.vasnabilisim.olive.core.property.PropertyGroup;
import com.vasnabilisim.olive.core.property.VerticalAlignmentProperty;

/**
 * @author Menderes Fatih GUVEN
 */
public class HorizontalLayout extends LinearLayout {
	private static final long serialVersionUID = 5047114039553542348L;
	
	protected HorizontalAlignmentProperty alignmentProperty;
	
	public HorizontalLayout() {
		super();
		this.alignmentProperty = PropertyFactoryX.createHorizontalAlignmentProperty("Content", "Alignment", "Alignment", "Alignment of sub elements", HorizontalAlignment.Leading);
		this.properties.add(this.alignmentProperty);
	}
	
	public HorizontalLayout(HorizontalLayout source) {
		super(source);
		this.alignmentProperty = source.alignmentProperty.clone();
		this.properties.add(this.alignmentProperty);
	}
	
	@Override
	public HorizontalLayout clone() {
		return new HorizontalLayout(this);
	}
	
	public HorizontalAlignment getAlignment() {
		return alignmentProperty.getValue();
	}
	
	public void setAlignment(HorizontalAlignment value) {
		alignmentProperty.setValue(value);
	}
	
	@Override
	public Dimension calcMinimumContentSize(DocumentContext context) {
		Dimension size = Dimension.Zero.clone();
		for(DesignNode child : children) {
			Dimension childSize = calcMinimumSizeOf(context, child);
			Insets margin = ((HorizontalLayoutConstraint)child.layoutConstraint).getMargin();
			size.w += childSize.w + margin.l + margin.r;
			size.h = Math.max(size.h, childSize.h + margin.t + margin.b);
		}
		return size;
	}
	
	@Override
	public void validateBounds(DocumentContext context) {
		if(!this.layoutConstraint.isValid())
			return;
		calcMinimumSize(context);
		switch (getAlignment()) {
		case Left:
			validateBounds_Left(context);
			break;
		case Leading:
			if(context.isRtl()) validateBounds_Right(context); else validateBounds_Left(context);
			break;
		case Right:
			validateBounds_Right(context);
			break;
		case Trailing:
			if(context.isRtl()) validateBounds_Left(context); else validateBounds_Right(context);
			break;
		case Center:
			validateBounds_Center(context);
			break;
		}
		validateChildrenBounds(context);
	}
	
	protected void validateBounds_Left(DocumentContext context) {
		Rectangle thisBounds = this.layoutConstraint.calculatedBounds;
		Insets thisInsets = getBorder().thickness.merge(getPadding());
		float width = thisBounds.w;
		width -= thisInsets.l;
		for(DesignNode child : children) {
			HorizontalLayoutConstraint constraint = (HorizontalLayoutConstraint) child.layoutConstraint;
			width -= constraint.getMargin().l;
			width -= constraint.calculatedMinSize.w;
			width -= constraint.getMargin().r;
		}
		width -= thisInsets.r;
		if(width < 0f)
			width = 0f;

		float height = thisBounds.h;
		height -= thisInsets.t;
		height -= thisInsets.b;
		if(height < 0f)
			height = 0f;
		
		float x = thisInsets.l;
		for(DesignNode child : children) {
			HorizontalLayoutConstraint constraint = (HorizontalLayoutConstraint) child.layoutConstraint;
			DimensionPolicy widthPolicy = constraint.getWidthPolicy();
			float widthFillRatio = constraint.getWidth();
			DimensionPolicy heightPolicy = constraint.getHeightPolicy();
			float heightFillRatio = constraint.getHeight();
			Insets margin = constraint.getMargin();
			VerticalAlignment alignment = constraint.getAlignment();
			
			Rectangle childBounds = constraint.calculatedBounds = Rectangle.Zero.clone();
			Dimension childMinSize = constraint.calculatedMinSize;
			
			x += margin.l;
			childBounds.x = x;
			if(widthPolicy == DimensionPolicy.Fill)
				childBounds.w = widthFillRatio * width;
			else
				childBounds.w = childMinSize.w;
			x += childBounds.w;
			x += margin.r;

			if(heightPolicy == DimensionPolicy.Fill)
				childBounds.h = heightFillRatio * height;
			else
				childBounds.h = childMinSize.h;
			
			if(alignment == VerticalAlignment.Top)
				childBounds.y = thisInsets.t + margin.t;
			else if(alignment == VerticalAlignment.Bottom)
				childBounds.y = thisBounds.h - thisInsets.b - margin.b - childBounds.h;
			else if(alignment == VerticalAlignment.Center)
				childBounds.y = (thisBounds.h - childBounds.h - margin.t - margin.b) * 0.5f;
		}
	}
	
	protected void validateBounds_Right(DocumentContext context) {
		validateBounds_Left(context);
		//TODO imlement
		/*
		float width = this.bounds.w;
		for(DesignNode child : children) {
			HorizontalLayoutConstraint constraint = (HorizontalLayoutConstraint) child.getLayoutConstraint();
			if(constraint.getWidthPolicy() == DimensionPolicy.Fixed)
				width -= constraint.getWidth();
			width -= constraint.getMargin().l;
			width -= constraint.getMargin().r;
		}
		if(width < 0f)
			width = 0f;
		float x = this.bounds.w;
		for(DesignNode child : children) {
			HorizontalLayoutConstraint constraint = (HorizontalLayoutConstraint) child.getLayoutConstraint();
			DimensionPolicy widthPolicy = constraint.getWidthPolicy();
			float widthFillRatio = constraint.getWidth();
			DimensionPolicy heightPolicy = constraint.getHeightPolicy();
			float heightFillRatio = constraint.getHeight();
			Insets margin = constraint.getMargin();
			VerticalAlignment alignment = constraint.getAlignment();
			
			x -= margin.r;
			if(widthPolicy == DimensionPolicy.Fixed)
				child.bounds.w = constraint.getWidth();
			else if(widthPolicy == DimensionPolicy.Fill)
				child.bounds.w = widthFillRatio * width;
			x -= child.bounds.w;
			child.bounds.x = x;
			x -= margin.l;
			
			if(heightPolicy == DimensionPolicy.Fixed)
				child.bounds.h = constraint.getHeight();
			else if(heightPolicy == DimensionPolicy.Fill)
				child.bounds.h = heightFillRatio * this.bounds.h; 
			
			if(alignment == VerticalAlignment.Top)
				child.bounds.y = margin.t;
			else if(alignment == VerticalAlignment.Bottom)
				child.bounds.y = this.bounds.h - margin.b - child.bounds.h;
			else if(alignment == VerticalAlignment.Center)
				child.bounds.y = (this.bounds.h - child.bounds.h - margin.t - margin.b) * 0.5f;
		}
		*/
	}
	
	protected void validateBounds_Center(DocumentContext context) {
		validateBounds_Left(context);
		//TODO imlement
		/*
		float width = this.bounds.w;
		for(DesignNode child : children) {
			HorizontalLayoutConstraint constraint = (HorizontalLayoutConstraint) child.getLayoutConstraint();
			if(constraint.getWidthPolicy() == DimensionPolicy.Fixed)
				width -= constraint.getWidth();
			width -= constraint.getMargin().l;
			width -= constraint.getMargin().r;
		}
		float x = width * 0.5f;
		if(width < 0f)
			width = 0f;
		for(DesignNode child : children) {
			HorizontalLayoutConstraint constraint = (HorizontalLayoutConstraint) child.getLayoutConstraint();
			if(constraint.getWidthPolicy() == DimensionPolicy.Fill)
				x -= width * constraint.getWidth() * 0.5f;
		}
		for(DesignNode child : children) {
			HorizontalLayoutConstraint constraint = (HorizontalLayoutConstraint) child.getLayoutConstraint();
			DimensionPolicy widthPolicy = constraint.getWidthPolicy();
			float widthFillRatio = constraint.getWidth();
			DimensionPolicy heightPolicy = constraint.getHeightPolicy();
			float heightFillRatio = constraint.getHeight();
			Insets margin = constraint.getMargin();
			VerticalAlignment alignment = constraint.getAlignment();
			
			x += margin.l;
			child.bounds.x = x;
			if(widthPolicy == DimensionPolicy.Fixed)
				child.bounds.w = constraint.getWidth();
			else if(widthPolicy == DimensionPolicy.Fill)
				child.bounds.w = widthFillRatio * width;
			x += child.bounds.w;
			x += margin.r;
			
			if(heightPolicy == DimensionPolicy.Fixed)
				child.bounds.h = constraint.getHeight();
			else if(heightPolicy == DimensionPolicy.Fill)
				child.bounds.h = heightFillRatio * this.bounds.h; 
			
			if(alignment == VerticalAlignment.Top)
				child.bounds.y = margin.t;
			else if(alignment == VerticalAlignment.Bottom)
				child.bounds.y = this.bounds.h - margin.b - child.bounds.h;
			else if(alignment == VerticalAlignment.Center)
				child.bounds.y = (this.bounds.h - child.bounds.h - margin.t - margin.b) * 0.5f;
		}
		*/
	}

	@Override
	protected HorizontalLayoutConstraint createLayoutConstraint() {
		return new HorizontalLayoutConstraint();
	}

	@Override
	protected HorizontalLayoutConstraint createLayoutConstraint(Properties p) {
		return new HorizontalLayoutConstraint(p.getGroup("Layout"));
	}

	public static class HorizontalLayoutConstraint extends LinearLayoutConstraint {
		private static final long serialVersionUID = 1137084422668397381L;

		protected VerticalAlignmentProperty alignmentProperty;

		public HorizontalLayoutConstraint() {
			super();
			this.alignmentProperty = PropertyFactoryX.createVerticalAlignmentProperty("Layout", "Alignment", "Alignment", "Vertical alignment", VerticalAlignment.Top);
			this.propertyGroup.add(this.alignmentProperty);
		}
		
		public HorizontalLayoutConstraint(PropertyGroup propertyGroup) {
			super();
			this.alignmentProperty = propertyGroup.get("Alignment", VerticalAlignmentProperty.class);
			this.propertyGroup.add(this.alignmentProperty);
		}

		public HorizontalLayoutConstraint(HorizontalLayoutConstraint source) {
			super(source);
			this.alignmentProperty = source.alignmentProperty.clone();
			this.propertyGroup.add(this.alignmentProperty);
		}
		
		@Override
		public HorizontalLayoutConstraint clone() {
			return new HorizontalLayoutConstraint(this);
		}
		
		public VerticalAlignment getAlignment() {
			return alignmentProperty.getValue();
		}
		
		public void setAlignment(VerticalAlignment value) {
			alignmentProperty.setValue(value);
		}
	}
}
