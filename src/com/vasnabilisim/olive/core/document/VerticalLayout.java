package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.DimensionPolicy;
import com.vasnabilisim.olive.core.geom.HorizontalAlignment;
import com.vasnabilisim.olive.core.geom.Insets;
import com.vasnabilisim.olive.core.geom.Rectangle;
import com.vasnabilisim.olive.core.geom.VerticalAlignment;
import com.vasnabilisim.olive.core.property.HorizontalAlignmentProperty;
import com.vasnabilisim.olive.core.property.Properties;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;
import com.vasnabilisim.olive.core.property.PropertyGroup;
import com.vasnabilisim.olive.core.property.VerticalAlignmentProperty;

/**
 * @author Menderes Fatih GUVEN
 */
public class VerticalLayout extends LinearLayout {
	private static final long serialVersionUID = 1281212845865933597L;

	protected VerticalAlignmentProperty alignmentProperty;

	public VerticalLayout() {
		super();
		this.alignmentProperty = PropertyFactoryX.createVerticalAlignmentProperty("Content", "Alignment", "Alignment", "Alignment of sub elements", VerticalAlignment.Top);
		this.properties.add(this.alignmentProperty);
	}
	
	public VerticalLayout(VerticalLayout source) {
		super(source);
		this.alignmentProperty = source.alignmentProperty.clone();
		this.properties.add(this.alignmentProperty);
	}
	
	@Override
	public VerticalLayout clone() {
		return new VerticalLayout(this);
	}
	
	public VerticalAlignment getAlignment() {
		return alignmentProperty.getValue();
	}
	
	public void setAlignment(VerticalAlignment value) {
		alignmentProperty.setValue(value);
	}
	
	@Override
	public Dimension calcMinimumContentSize(DocumentContext context) {
		Dimension size = Dimension.Zero.clone();
		for(DesignNode child : children) {
			Dimension childSize = calcMinimumSizeOf(context, child);
			Insets margin = ((VerticalLayoutConstraint)child.layoutConstraint).getMargin();
			size.h += childSize.h + margin.t + margin.b;
			size.w = Math.max(size.w, childSize.w + margin.l + margin.r);
		}
		return size;
	}
	
	@Override
	public void validateBounds(DocumentContext context) {
		if(!this.layoutConstraint.isValid())
			return;
		calcMinimumSize(context);
		switch (getAlignment()) {
		case Top:
			validateBounds_Top(context);
			break;
		case Bottom:
			validateBounds_Bottom(context);
			break;
		case Center:
			validateBounds_Center(context);
			break;
		}
		validateChildrenBounds(context);
	}

	protected void validateBounds_Top(DocumentContext context) {
		Rectangle thisBounds = this.layoutConstraint.calculatedBounds;
		Insets thisInsets = getBorder().thickness.merge(getPadding());
		float height = thisBounds.h;
		height -= thisInsets.t;
		for(DesignNode child : children) {
			VerticalLayoutConstraint constraint = (VerticalLayoutConstraint) child.layoutConstraint;
			height -= constraint.getMargin().t;
			height -= constraint.calculatedMinSize.h;
			height -= constraint.getMargin().t;
		}
		height -= thisInsets.b;
		if(height < 0f)
			height = 0f;

		float width = thisBounds.w;
		width -= thisInsets.l;
		width -= thisInsets.r;
		if(width < 0f)
			width = 0f;
		
		float y = thisInsets.t;
		for(DesignNode child : children) {
			VerticalLayoutConstraint constraint = (VerticalLayoutConstraint) child.layoutConstraint;
			DimensionPolicy heightPolicy = constraint.getHeightPolicy();
			float heightFillRatio = constraint.getHeight();
			DimensionPolicy widthPolicy = constraint.getWidthPolicy();
			float widthFillRatio = constraint.getWidth();
			Insets margin = constraint.getMargin();
			HorizontalAlignment alignment = context.decide(constraint.getAlignment());
			
			Rectangle childBounds = constraint.calculatedBounds = Rectangle.Zero.clone();
			Dimension childMinSize = constraint.calculatedMinSize;
			
			y += margin.t;
			childBounds.y = y;
			if(heightPolicy == DimensionPolicy.Fill)
				childBounds.h = heightFillRatio * height;
			else
				childBounds.h = childMinSize.h;
			y += childBounds.h;
			y += margin.b;

			if(widthPolicy == DimensionPolicy.Fill)
				childBounds.w = widthFillRatio * width;
			else
				childBounds.w = childMinSize.w;
			
			if(alignment == HorizontalAlignment.Left)
				childBounds.x = thisInsets.l + margin.l;
			else if(alignment == HorizontalAlignment.Right)
				childBounds.x = thisBounds.w - thisInsets.r - margin.r - childBounds.w;
			else //if(alignment == HorizontalAlignment.Center)
				childBounds.x = (thisBounds.w - childBounds.w - margin.l - margin.r) * 0.5f;
		}
	}

	protected void validateBounds_Bottom(DocumentContext context) {
		//TODO implement
		validateBounds_Top(context);
	}

	protected void validateBounds_Center(DocumentContext context) {
		//TODO implement
		validateBounds_Top(context);
	}

	@Override
	protected VerticalLayoutConstraint createLayoutConstraint() {
		return new VerticalLayoutConstraint();
	}

	@Override
	protected VerticalLayoutConstraint createLayoutConstraint(Properties p) {
		return new VerticalLayoutConstraint(p.getGroup("Layout"));
	}

	public static class VerticalLayoutConstraint extends LinearLayoutConstraint {
		private static final long serialVersionUID = -5774153378496690965L;

		protected HorizontalAlignmentProperty alignmentProperty;

		public VerticalLayoutConstraint() {
			super();
			this.alignmentProperty = PropertyFactoryX.createHorizontalAlignmentProperty("Layout", "Alignment", "Alignment", "Horizontal alignment", HorizontalAlignment.Leading);
			this.propertyGroup.add(this.alignmentProperty);
		}
		
		public VerticalLayoutConstraint(PropertyGroup propertyGroup) {
			super();
			this.alignmentProperty = propertyGroup.get("Alignment", HorizontalAlignmentProperty.class);
			this.propertyGroup.add(this.alignmentProperty);
		}

		public VerticalLayoutConstraint(VerticalLayoutConstraint source) {
			super(source);
			this.alignmentProperty = source.alignmentProperty.clone();
			this.propertyGroup.add(this.alignmentProperty);
		}
		
		@Override
		public VerticalLayoutConstraint clone() {
			return new VerticalLayoutConstraint(this);
		}
		
		public HorizontalAlignment getAlignment() {
			return alignmentProperty.getValue();
		}
		
		public void setAlignment(HorizontalAlignment value) {
			alignmentProperty.setValue(value);
		}
	}
}
