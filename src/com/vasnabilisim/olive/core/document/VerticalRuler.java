package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.paint.Canvas;

/**
 * Vertical ruler.
 * 
 * @author Menderes Fatih GUVEN
 */
public class VerticalRuler extends Ruler {
	private static final long serialVersionUID = -3186972167502743027L;

	/**
	 * Width of the ruler.
	 */
	protected float width = 10;

	/**
	 * Height of the ruler.
	 */
	protected float height = 10;

	/**
	 * Default constructor.
	 */
	public VerticalRuler() {
	}
	
	/**
	 * Copy constructor();
	 * @param source
	 */
	public VerticalRuler(VerticalRuler source) {
		super(source);
		this.width = source.width;
		this.height = source.height;
	}
	
	@Override
	public VerticalRuler clone() {
		return new VerticalRuler(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.document.Ruler#getWidth()
	 */
	@Override
	public float getWidth() {
		return width;
	}

	/**
	 * Sets the width.
	 * 
	 * @param width
	 */
	public void setWidth(float width) {
		this.width = width;
		fireDocumentModifiedEvent("width");
	}

	/**
	 * @see com.vasnabilisim.olive.core.document.Ruler#getHeight()
	 */
	@Override
	public float getHeight() {
		return height;
	}

	/**
	 * Sets the height.
	 * 
	 * @param height
	 */
	public void setHeight(int height) {
		this.height = height;
		fireDocumentModifiedEvent("height");
	}

	/**
	 * Paints the ruler to given graphics device.
	 * 
	 * @param g
	 */
	public void paint(Canvas g) {
		/*
		float width = getWidth();
		float height = getHeight();

		// Paint background.
		g.setColor(getBackground());
		g.fillRect(0, 0, width, height);

		// paint border
		g.setColor(getDarkBackground());
		g.drawRect(0, 0, width, height);

		// paint units
		g.setColor(getUnitColor());
		float y = 0f;
		float pixelCount = PageUnit.Inch.pixelCount();
		if (getPage().getUnit() == PageUnit.Cm)
			pixelCount = PageUnit.Cm.pixelCount();
		while (y <= height) {
			g.fillRect(5, y, 10, 1);
			g.fillRect(7, (y + pixelCount / 2), 6, 1);
			y += pixelCount;
		}

		// Paint mark.
		g.setPaint(getMarkColor());
		g.fillRect(1, markStart, getWidth(), markLength);
		*/
	}
}
