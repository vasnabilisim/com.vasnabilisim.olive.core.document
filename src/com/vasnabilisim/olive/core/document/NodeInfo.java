package com.vasnabilisim.olive.core.document;

/**
 * Item info class. Designer tool box filled from item info list.
 * 
 * @author Menderes Fatih GUVEN
 */
public class NodeInfo implements
		Comparable<NodeInfo>,  
		java.lang.Cloneable,
		com.vasnabilisim.util.Cloneable<NodeInfo> 
{

	private String name = null;
	private String titleKey = null;
	private String iconKey = null;
	private String componentClassName = null;
	private String rendererClassName = null;
	private boolean toolBoxVisible = true;

	/**
	 * Default constructor
	 */
	public NodeInfo() {
	}

	/**
	 * Value constructor.
	 * 
	 * @param name
	 * @param titleKey
	 * @param iconKey
	 * @param componentClassName
	 * @param rendererClassName
	 * @param toolBoxVisible
	 */
	public NodeInfo(String name, String titleKey, String iconKey,
			String componentClassName, String rendererClassName,
			boolean toolBoxVisible) {
		this.name = name;
		this.titleKey = titleKey;
		this.iconKey = iconKey;
		this.componentClassName = componentClassName;
		this.rendererClassName = rendererClassName;
		this.toolBoxVisible = toolBoxVisible;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public NodeInfo(NodeInfo source) {
		this.name = source.name;
		this.titleKey = source.titleKey;
		this.iconKey = source.iconKey;
		this.componentClassName = source.componentClassName;
		this.rendererClassName = source.rendererClassName;
		this.toolBoxVisible = source.toolBoxVisible;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name + " - " + componentClassName;
	}

	/**
	 * Returns title key.
	 * 
	 * @return
	 */
	public String getTitleKey() {
		return titleKey;
	}

	/**
	 * Sets title key.
	 * 
	 * @param titleKey
	 */
	public void setTitleKey(String titleKey) {
		this.titleKey = titleKey;
	}

	/**
	 * Returns component class name.
	 * 
	 * @return
	 */
	public String getComponentClassName() {
		return componentClassName;
	}

	/**
	 * Sets component class name.
	 * 
	 * @param componentClassName
	 */
	public void setComponentClassName(String componentClassName) {
		this.componentClassName = componentClassName;
	}

	/**
	 * Returns renderer class name.
	 * 
	 * @return
	 */
	public String getRendererEngineClassName() {
		return rendererClassName;
	}

	/**
	 * Sets renderer class name.
	 * 
	 * @param rendererEngineClassName
	 */
	public void setRendererEngineClassName(String rendererEngineClassName) {
		this.rendererClassName = rendererEngineClassName;
	}

	/**
	 * Returns icon key.
	 * 
	 * @return
	 */
	public String getIconKey() {
		return iconKey;
	}

	/**
	 * Sets icon key.
	 * 
	 * @param iconKey
	 */
	public void setIconKey(String iconKey) {
		this.iconKey = iconKey;
	}

	/**
	 * Returns name.
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets name.
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Is toolbox visible.
	 * 
	 * @return
	 */
	public boolean isToolBoxVisible() {
		return toolBoxVisible;
	}

	/**
	 * Sets tool box visible.
	 * 
	 * @param toolBoxVisiable
	 */
	public void setToolBoxVisible(boolean toolBoxVisible) {
		this.toolBoxVisible = toolBoxVisible;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (this == o)
			return true;
		if (!(o instanceof NodeInfo))
			return false;
		NodeInfo other = (NodeInfo) o;
		return this.name.equals(other.name);
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(NodeInfo o) {
		if (o == null)
			return -1;
		if (this == o)
			return 0;
		return this.name.compareTo(o.name);
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return cloneObject();
	}

	/**
	 * @see com.bilgibiz.bizreport.core.Cloneable#cloneObject()
	 */
	@Override
	public NodeInfo cloneObject() {
		return new NodeInfo(this);
	}

}
