package com.vasnabilisim.olive.core.document;

import java.util.EventObject;

/**
 * Event used to fire property changes.
 * @author Menderes Fatih GUVEN
 */
public class DocumentEvent extends EventObject {
	private static final long serialVersionUID = -2099240237971981659L;

	protected String property;

	/**
	 * Source-Property constructor.
	 * @param source
	 * @param property
	 */
	public DocumentEvent(Node source, String property) {
		super(source);
		this.property = property;
	}

	/**
	 * Returns the node which has changed.
	 * @return
	 */
	public Node getNode() {
		return (Node)getSource();
	}
	
	/**
	 * Returns the property which is changed.
	 * @return
	 */
	public String getProperty() {
		return property;
	}
}

