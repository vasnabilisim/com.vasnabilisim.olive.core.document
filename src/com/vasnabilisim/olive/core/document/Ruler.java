package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.paint.Color;
import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.paper.PaperConstants;

/**
 * Base class of all rulers.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class Ruler extends Node implements Constants, PaperConstants {
	private static final long serialVersionUID = 8510486234031754965L;

	/**
	 * Default constructor.
	 */
	protected Ruler() {
		super();
	}
	
	/**
	 * Copy constructor.
	 * @param source
	 */
	protected Ruler(Ruler source) {
		super(source);
	}
	
	/**
	 * Page to paint ruler.
	 */
	protected Paper paper = Paper.Default.clone();

	/**
	 * Background color.
	 */
	protected Color background = RULER_BACKGROUND;

	/**
	 * Back ground dark color.
	 */
	protected Color darkBackground = RULER_DARK_BACKGROUND;

	/**
	 * Unit color.
	 */
	protected Color unitColor = RULER_UNIT_COLOR;

	/**
	 * Margin color.
	 */
	protected Color marginColor = MARGIN_COLOR;

	/**
	 * Margin color.
	 */
	protected Color markColor = RULER_MARK_COLOR;

	/**
	 * Mark start x coordinate.
	 */
	protected float markStart = -1f;

	/**
	 * Mark length.
	 */
	protected float markLength = -1f;

	/**
	 * Returns the page.
	 * 
	 * @return
	 */
	public Paper getPage() {
		return paper;
	}

	/**
	 * Sets the page
	 * 
	 * @param page
	 */
	public void setPage(Paper page) {
		this.paper = page;
		fireDocumentModifiedEvent("page");
	}

	/**
	 * Returns the background.
	 * 
	 * @return
	 */
	public Color getBackground() {
		return background;
	}

	/**
	 * Sets the background.
	 * 
	 * @param background
	 */
	public void setBackground(Color background) {
		this.background = background;
		fireDocumentModifiedEvent("background");
	}

	/**
	 * Returns the dark back ground color.
	 * 
	 * @return
	 */
	public Color getDarkBackground() {
		return darkBackground;
	}

	/**
	 * Sets the dark back ground color.
	 * 
	 * @param color
	 */
	public void setDarkBackground(Color color) {
		darkBackground = color;
		fireDocumentModifiedEvent("darkbackground");
	}

	/**
	 * Returns the unit color.
	 * 
	 * @return
	 */
	public Color getUnitColor() {
		return unitColor;
	}

	/**
	 * Sets the unit color.
	 * 
	 * @param color
	 */
	public void setUnitColor(Color color) {
		unitColor = color;
		fireDocumentModifiedEvent("unitcolor");
	}

	/**
	 * Returns the margin color.
	 * 
	 * @return
	 */
	public Color getMarginColor() {
		return marginColor;
	}

	/**
	 * Sets the margin color.
	 * 
	 * @param color
	 */
	public void setMarginColor(Color color) {
		marginColor = color;
		fireDocumentModifiedEvent("margincolor");
	}

	/**
	 * Returns the mark color.
	 * 
	 * @return
	 */
	public Color getMarkColor() {
		return markColor;
	}

	/**
	 * Sets the mark color.
	 * 
	 * @param markColor
	 */
	public void setMarkColor(Color markColor) {
		this.markColor = markColor;
		fireDocumentModifiedEvent("markcolor");
	}

	/**
	 * Marks the ruler.
	 * 
	 * @param markStart
	 * @param markLength
	 */
	public void mark(float markStart, float markLength) {
		this.markStart = markStart;
		this.markLength = markLength;
		fireDocumentModifiedEvent("mark");
	}

	/**
	 * Clears the mark from ruler.
	 */
	public void unmark() {
		markStart = markLength = -1f;
		fireDocumentModifiedEvent("mark");
	}

	/**
	 * Returns the width of the ruler.
	 * 
	 * @return
	 */
	public abstract float getWidth();

	/**
	 * Returns the height of the ruler.
	 * 
	 * @return
	 */
	public abstract float getHeight();

	/**
	 * Returns the preferred size.
	 * 
	 * @return
	 */
	public Dimension getPreferredSize() {
		return new Dimension(getWidth(), getHeight());
	}
}
