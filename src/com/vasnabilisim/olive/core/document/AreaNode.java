package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.paint.Color;
import com.vasnabilisim.olive.core.property.ColorProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;

/**
 * All 2 dimensional design items are of this type.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class AreaNode extends DesignNode {
	private static final long serialVersionUID = -5396309637117554420L;

	protected ColorProperty backgroundProperty;
	
	public AreaNode() {
		super();
		this.backgroundProperty = PropertyFactoryX.createColorProperty("Content", "Background", "Background", "Background color of element", Color.Transparent);
		this.properties.add(this.backgroundProperty);
	}

	protected AreaNode(AreaNode source) {
		super(source);
		this.backgroundProperty = source.backgroundProperty.clone();
		this.properties.add(this.backgroundProperty);
	}
	
	@Override
	public abstract AreaNode clone();

	public Color getBackground() {
		return backgroundProperty.getValue();
	}
	
	public void setBackground(Color value) {
		backgroundProperty.setValue(value);
	}
}
