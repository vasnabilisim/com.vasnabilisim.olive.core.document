package com.vasnabilisim.olive.core.document;

import java.io.Serializable;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.Rectangle;
import com.vasnabilisim.olive.core.property.PropertyGroup;

public abstract class LayoutConstraint implements Serializable, Cloneable {
	private static final long serialVersionUID = 5653011203285223547L;

	protected PropertyGroup propertyGroup;
	
	//Not a property used to hold the actual size of the node. 
	//Calculated and set by layout process. 
	//Cached in order not to recalculate every time needed.
	//Border and padding included. Margin excluded.
	//Bounds is in paper units.
	protected Dimension calculatedMinSize;
	
	//Not a property used to hold the actual bounds of the node. 
	//Calculated and set by layout process. 
	//Cached in order not to recalculate every time needed.
	//Border and padding included. Margin excluded.
	//Bounds is in paper units.
	protected Rectangle calculatedBounds;
	
	protected LayoutConstraint() {
		propertyGroup = new PropertyGroup("Layout Constraint");
	}

	/*
	protected LayoutConstraint(PropertyGroup propertyGroup) {
		this.propertyGroup = propertyGroup;
	}
	*/
	
	protected LayoutConstraint(LayoutConstraint source) {
		this.propertyGroup = new PropertyGroup("Layout Constraint");
		this.calculatedBounds = source.calculatedBounds == null ? null : source.calculatedBounds.clone();
	}
	
	@Override
	public abstract LayoutConstraint clone();
	
	public PropertyGroup getPropertyGroup() {
		return propertyGroup.cloneObject();
	}
	
	public boolean isValid() {
		return this.calculatedBounds == null;
	}
	
	public void invalidate() {
		this.calculatedMinSize = null;
		this.calculatedBounds = null;
	}
	
	public static class EmptyLayoutConstraint extends LayoutConstraint {
		private static final long serialVersionUID = -5191065147362908752L;
		private EmptyLayoutConstraint() {}
		@Override
		public EmptyLayoutConstraint clone() {
			return this;
		}
	}
	public static EmptyLayoutConstraint Empty = new EmptyLayoutConstraint();
}
