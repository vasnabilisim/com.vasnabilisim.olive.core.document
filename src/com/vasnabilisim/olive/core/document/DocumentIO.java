package com.vasnabilisim.olive.core.document;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.TreeMap;

import org.w3c.dom.Element;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.core.Logger;
import com.vasnabilisim.olive.core.property.Properties;
import com.vasnabilisim.olive.core.property.Property;
import com.vasnabilisim.olive.core.property.PropertyException;
import com.vasnabilisim.olive.core.property.PropertyGroup;
import com.vasnabilisim.xml.XmlException;
import com.vasnabilisim.xml.XmlParser;

/**
 * @author Menderes Fatih GUVEN
 */
public final class DocumentIO {

	private DocumentIO() {
	}
	
	static TreeMap<String, Class<? extends Node>> nodeClasses = new TreeMap<>();
	
	static {
		registerNodeClass("Report", Document.class);
		registerNodeClass("DynamicText", DynamicText.class);
		registerNodeClass("HorizontalLayout", HorizontalLayout.class);
		registerNodeClass("Part", Part.class);
		registerNodeClass("StaticImage", StaticImage.class);
		registerNodeClass("StaticText", StaticText.class);
		registerNodeClass("VerticalLayout", VerticalLayout.class);
		registerNodeClass("XYLayout", XYLayout.class);
	}

	public static <N extends Node> void registerNodeClass(String name, Class<N> nodeClass) {
		nodeClasses.put(name, nodeClass);
	}
	
	public static Class<? extends Node> getNodeClass(String name) {
		return nodeClasses.get(name);
	}
	
	public static Node createNode(String name) {
		Class<? extends Node> nodeClass = getNodeClass(name);
		if(nodeClass == null)
			return null;
		try {
			return nodeClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			Logger.log(e);
			return null;
		}
	}

	public static Node read(String fileName) throws BaseException {
		return read(new File(fileName));
	}

	public static Node read(File file) throws BaseException {
		try {
			return read(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			throw new ErrorException(e);
		}
	}

	public static Node read(InputStream in) throws BaseException {
		try {
			return read(new InputStreamReader(in, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new ErrorException(e);
		}
	}

	public static Node read(Reader reader) throws BaseException {
		org.w3c.dom.Document xmlDocument;
		try {
			xmlDocument = XmlParser.fromReaderToDocument(reader);
		} catch (XmlException e) {
			throw new ErrorException("Unable to parse document", e);
		}
		//TODO re-implement
		Element reportElement = xmlDocument.getDocumentElement();
		Node node = createNode(reportElement.getNodeName());
		readProperties(xmlDocument, reportElement, node);
		if(node instanceof Layout)
			readChildren(xmlDocument, reportElement, (Layout)node);
		return node;
	}

	static void readChildren(org.w3c.dom.Document xmlDocument, Element layoutElement, Layout layout) {
		Element childrenElement = XmlParser.findChildElement(layoutElement, "Children");
		if(childrenElement == null)
			return;
		List<Element> nodeElements = XmlParser.allChildElements(childrenElement);
		for(Element nodeElement : nodeElements) {
			Node node = createNode(nodeElement.getNodeName());
			if(node == null || !(node instanceof DesignNode))
				continue;
			layout.addChild((DesignNode)node);
			readProperties(xmlDocument, nodeElement, node);
			
			if(node instanceof Layout)
				readChildren(xmlDocument, nodeElement, (Layout) node);
		}
	}

	static void readProperties(org.w3c.dom.Document xmlDocument, Element element, Node node) {
		Properties properties = node.getProperties();
		for(PropertyGroup propertyGroup : properties.getGroups()) {
			for(Property<?> property : propertyGroup.getProperties()) {
				Element childElement = XmlParser.findChildElement(element, property.getName());
				if(childElement == null)
					continue;
				try {
					property.readXmlElement(xmlDocument, childElement);
				} catch (PropertyException e) {
					Logger.log(e);
				}
			}
		}
		node.setProperties(properties);
	}
}
