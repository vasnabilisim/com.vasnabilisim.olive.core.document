package com.vasnabilisim.olive.core.document;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.property.Properties;

/**
 * All design items are of this type.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class DesignNode extends Node {
	private static final long serialVersionUID = 9092408566224362141L;

	protected NodeInfo info = null;

	protected Layout parent;
	
	protected LayoutConstraint layoutConstraint;

	//protected FloatProperty widthProperty;
	//protected FloatProperty heightProperty;

	protected DesignNode() {
		super();
		this.layoutConstraint = LayoutConstraint.Empty;
	}

	protected DesignNode(DesignNode source) {
		super(source);
		this.layoutConstraint = layoutConstraint.clone();
	}
	
	@Override
	public abstract DesignNode clone();

	@Override
	public Properties getProperties() {
		Properties p = super.getProperties();
		p.addGroup(layoutConstraint.getPropertyGroup());
		return p;
	}

	/**
	 * Returns the parent.
	 * 
	 * @return
	 */
	public Layout getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 * 
	 * @param parent
	 */
	public void setParent(Layout parent) {
		this.parent = parent;
	}
	
	/**
	 * Returns layout constraint which defines the layout of this none in its parent.
	 * @return
	 */
	public LayoutConstraint getLayoutConstraint() {
		return layoutConstraint;
	}

	/**
	 * Sets layout constraint which defines the layout of this none in its parent.
	 * @param layoutConstraint
	 */
	public void setLayoutConstraint(LayoutConstraint layoutConstraint) {
		this.layoutConstraint = layoutConstraint;
		fireDocumentModifiedEvent("layoutConstraint");
	}

	
	/**
	 * Returns the report component info.
	 * 
	 * @return
	 */
	public NodeInfo getInfo() {
		return info;
	}

	/**
	 * Sets the report component info.
	 * 
	 * @param info
	 */
	public void setInfo(NodeInfo info) {
		this.info = info;
	}

	/**
	 * Calculates minimum size required to show itself. 
	 * Also sets the result to layoutConstraint#calculatedMinSize
	 * Margin excluded.
	 * Border, padding included.
	 * @param context
	 * @return
	 */
	public abstract Dimension calcMinimumSize(DocumentContext context);
}
